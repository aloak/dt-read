//公共js，主要做表单验证，以及基本方法封装
const utils = {
  isNullOrEmpty: function (value) {
    //是否为空
    return value === null || value === "" || value === undefined ? true : false;
  },
  trim: function (value) {
    //去空格
    return value.replace(/(^\s*)|(\s*$)/g, "");
  },
  isMobile: function (value) {
    //是否为手机号
    return /^(?:13\d|14\d|15\d|16\d|17\d|18\d|19\d)\d{5}(\d{3}|\*{3})$/.test(
      value
    );
  },
  isFloat: function (value) {
    //金额，只允许保留两位小数
    return /^([0-9]*[.]?[0-9])[0-9]{0,1}$/.test(value);
  },
  isNum: function (value) {
    //是否全为数字
    return /^[0-9]+$/.test(value);
  },
  equalObj: function (obj1, obj2) {
    if (Object.keys(obj1).length != Object.keys(obj2).length) {
      return false;
    }
    let equal = true;
    for (let key in obj1) {
      if (obj1[key] != obj2[key]) {
        equal = false;
        break;
      }
    }
    return equal;
  },
  equalUrl: function (url1, url2) {
    return url1.substr(url1.indexOf("://")) == url2.substr(url2.indexOf("://"));
  },
  checkPwd: function (value) {
    //密码为8~20位数字和字母组合
    return /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,20}$/.test(value);
  },
  formatNum: function (num) {
    //格式化手机号码
    if (utils.isMobile(num)) {
      num = num.replace(/^(\d{3})\d{4}(\d{4})$/, "$1****$2");
    }
    return num;
  },
  rmoney: function (money) {
    //金额格式化
    return parseFloat(money)
      .toFixed(2)
      .toString()
      .split("")
      .reverse()
      .join("")
      .replace(/(\d{3})/g, "$1,")
      .replace(/\,$/, "")
      .split("")
      .reverse()
      .join("");
  },
  formatDate: function (formatStr, fdate) {
    //日期格式化
    if (fdate) {
      if (~fdate.indexOf(".")) {
        fdate = fdate.substring(0, fdate.indexOf("."));
      }
      fdate = fdate
        .toString()
        .replace("T", " ")
        .replace(/\-/g, "/");
      var fTime,
        fStr = "ymdhis";
      if (!formatStr) formatStr = "y-m-d h:i:s";
      if (fdate) fTime = new Date(fdate);
      else fTime = new Date();
      var month = fTime.getMonth() + 1;
      var day = fTime.getDate();
      var hours = fTime.getHours();
      var minu = fTime.getMinutes();
      var second = fTime.getSeconds();
      month = month < 10 ? "0" + month : month;
      day = day < 10 ? "0" + day : day;
      hours = hours < 10 ? "0" + hours : hours;
      minu = minu < 10 ? "0" + minu : minu;
      second = second < 10 ? "0" + second : second;
      var formatArr = [
        fTime.getFullYear().toString(),
        month.toString(),
        day.toString(),
        hours.toString(),
        minu.toString(),
        second.toString()
      ];
      for (var i = 0; i < formatArr.length; i++) {
        formatStr = formatStr.replace(fStr.charAt(i), formatArr[i]);
      }
      return formatStr;
    } else {
      return "";
    }
  },
  formatJson: (json) => {
    let formatted = '',     //转换后的json字符串
      padIdx = 0,         //换行后是否增减PADDING的标识
      PADDING = '  ';   //2个空格符
    /**
     * 将对象转化为string
     */
    if (typeof json !== 'string') {
      json = JSON.stringify(json);
    }
    /** 
     *利用正则类似将{'name':'ccy','age':18,'info':['address':'wuhan','interest':'playCards']}
     *---> \r\n{\r\n'name':'ccy',\r\n'age':18,\r\n
     *'info':\r\n[\r\n'address':'wuhan',\r\n'interest':'playCards'\r\n]\r\n}\r\n
     */
    /**
     * 修改 2020年2月27日
     * .replace(/([\{\}])/g, '\r\n$1\r\n') => .replace(/(\{)/g, '$1\r\n').replace(/(\})/g, '\r\n$1\r\n')
     * .replace(/([\[\]])/g, '\r\n$1\r\n') => .replace(/(\[)/g, '$1\r\n').replace(/(\])/g, '\r\n$1\r\n')
     * 添加"xxx[xx]xxx"中的[]不换行 .replace(/(\"\S*\[\s+.*\s+\]\s+.*\")/g,function($,$1,$2){return $1.replace(/\s+/g,'')})
     */
    json = json.replace(/(\{)/g, '$1\r\n')
      .replace(/(\})/g, '\r\n$1\r\n')
      .replace(/(\[)/g, '$1\r\n')
      .replace(/(\])/g, '\r\n$1\r\n')
      .replace(/(\,)/g, '$1\r\n')
      .replace(/(\r\n\r\n)/g, '\r\n')
      .replace(/\r\n\,/g, ',')
      .replace(/(\"\S*\[\s+.*\s+\]\s+.*\")/g, function ($, $1, $2) { return $1.replace(/\s+/g, '') });

    /** 
     * 根据split生成数据进行遍历，一行行判断是否增减PADDING
     * 
     * 修改 2020年2月27日
     * else if (node.match(/\}/) || node.match(/\]/)) => else if (node.match(/(\}|\},)$/) || node.match(/(\]|\],)$/))
     */
    (json.split('\r\n')).forEach(function (node, index) {
      var indent = 0,
        padding = '';
      if (node.match(/\{$/) || node.match(/\[$/)) indent = 1;
      else if (node.match(/(\}|\},)$/) || node.match(/(\]|\],)$/)) padIdx = (padIdx !== 0 ? --padIdx : padIdx);
      else indent = 0;
      for (var i = 0; i < padIdx; i++) padding += PADDING;
      formatted += padding + node + '\r\n';
      padIdx += indent;
      // console.log('index:' + index + ',indent:' + indent + ',padIdx:' + padIdx + ',node-->' + node);
    });

    return formatted;
  },
  parseJson: str => {
    if (typeof str !== 'string') {
      return ''
    }
    return str.replace(/\r\n/g, ' ')
  }
};

module.exports = {
  isNullOrEmpty: utils.isNullOrEmpty,
  trim: utils.trim,
  isMobile: utils.isMobile,
  isFloat: utils.isFloat,
  isNum: utils.isNum,
  equalObj: utils.equalObj,
  equalUrl: utils.equalUrl,
  checkPwd: utils.checkPwd,
  formatNum: utils.formatNum,
  rmoney: utils.rmoney,
  formatDate: utils.formatDate,
  formatJson: utils.formatJson,
  parseJson: utils.parseJson
};
